import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux'
import * as actions from './actions'
import Search from './components/search/search'
import Movie from './components/movie/movie'
import Timeline from './components/ui/timeline/timeline'
import Watched from './components/watched/watched';
import InitialComponent from './components/ui/initialComponent/initialComponent'
import Logo from './components/ui/logo/logo';

class App extends Component {
  render() {
    const { moviesList, watchedMovies, searchStringStatus, watchedMoviesId, errorMsg } = this.props
    return (
      <div>
        <div className="header">
          <Logo logoText={"MovieFlix"} />
          <Search
            searchMovie={(e) => this.props.onSearchMovieHanlder(e)}
            onChangeSearch={e => this.props.onChangeSearchHandler(e)} />
        </div>
        {searchStringStatus ?
          <Timeline /> :
          (moviesList === undefined )|| (Object.keys(moviesList).length === 0 && moviesList.constructor === Object) ?
            <InitialComponent text={"Search a movie"} error={errorMsg} /> :
            <Movie
              movies={moviesList}
              watchedMovie={(id) => this.props.onWatchedMovieHanlder(id)}
              watchedMoviesId={watchedMoviesId} />
        }
        <div className="footer">
          <h2>Watched Movies</h2>
          <Watched 
            watchedMovies={watchedMovies} 
            removeMovieFromWatched={id => this.props.onRemoveMovieFromWatched(id)}
            moviesLength = {watchedMovies.length > 5 ? true : false}/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    moviesList: state.movies.movies,
    watchedMovies: state.movies.watchedMovies,
    searchStringStatus: state.movies.searchStringStatus,
    watchedMoviesId: state.movies.watchedMoviesId,
    errorMsg: state.movies.errorMessage
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSearchMovieHanlder: (e) => dispatch(actions.fetchMovieList(e)),
    onWatchedMovieHanlder: id => dispatch(actions.watchedMovie(id)),
    onChangeSearchHandler: e => dispatch(actions.onChangeSearch(e)),
    onRemoveMovieFromWatched: id => dispatch(actions.onRemoveMovie(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
