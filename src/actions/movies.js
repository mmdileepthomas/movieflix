import * as actionTypes from './actionTypes'
import axios from 'axios'
// on search handler
export const onChangeSearch = e =>  dispatch => {
    if(e.target.value){
    return  dispatch({type:actionTypes.SEARCH_STRING_STATUS, payload:true}) 
    }
    return dispatch({type:actionTypes.SEARCH_STRING_STATUS, payload:false})
}

// fetching items
export const fetchMovieList = (e) => async dispatch => {
    if(e.key === 'Enter'){
        const res = await axios.get(`http://www.omdbapi.com/?t=${e.target.value}&apikey=aabca0d`)
        if(res.data.Response === "False"){
            return dispatch({type:actionTypes.FETCH_MOVIES, error:res.data.Error}) 
        }
       return dispatch({type:actionTypes.FETCH_MOVIES, payload:res.data})
    }else if(e.key === 'Backspace' ||  e.target.value === ""){
        dispatch({type:actionTypes.FETCH_MOVIES, payload:{}})
    }
}
// setting the watched movie
export const watchedMovie = id =>  async dispatch => {
    const res = await axios.get(`http://www.omdbapi.com/?i=${id}&apikey=aabca0d`)
    dispatch({type:actionTypes.WATCHED_MOVIE, payload:res.data})
}
// removing the watched movie
export const onRemoveMovie = id => dispatch => {
    dispatch({type:actionTypes.REMOVE_WATCHED_MOVIE, payload: id})
}