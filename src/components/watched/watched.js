import React, { Component } from 'react'
import './watched.css'
import InitialComponent from '../ui/initialComponent/initialComponent'

class Watched extends Component {
    render() {
        let moviesList = Object.values(this.props.watchedMovies.reduce((acc, cur) => Object.assign(acc, { [cur.imdbID]: cur }), {}));
        return (
            <div className="watched-movies-container">
                {
                    moviesList.length > 0?  moviesList.map((o, key) => {
                        return (
                            <div className="watched-movie" key={key}>
                                <div className="movie-poster">
                                    <img
                                        src={o.Poster === "N/A" || !o.Poster ? require('../../assets/movieIcon.png') : o.Poster}
                                        alt={key}
                                        onClick={() => this.props.removeMovieFromWatched(o.imdbID)} />
                                </div>
                            </div>
                        )
                    }):
                    <InitialComponent text={"No Movies added to Watch List"} height={0} margin={"15% 0 0 15%"}/>
                }
            </div>
        )
    }
}




export default Watched