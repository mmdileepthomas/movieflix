import React from 'react'
import './search.css'

const search = props => {
    return(
        <div className="search-container">
            <input 
                type="text" 
                placeholder={"Search For Movie and Hit Enter"} 
                onKeyDown={e => props.searchMovie(e)}
                onChange = {e => props.onChangeSearch(e)}/>
        </div>
    )
}
export default search