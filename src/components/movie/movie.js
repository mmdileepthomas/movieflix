import React, { Component } from 'react';
import './movie.css'

class Movie extends Component {
    render() {
        const { movies, watchedMoviesId } = this.props
        let watchedStatus = watchedMoviesId.find(o => o === movies.imdbID)
        return (
            <div className="movie-container">
                <div className="searched-movie">
                    <div className="movie-poster-container">
                        <div className="image">
                            <div className="image-shimmer">
                                <div className="movie-data">
                                    <img
                                        src={!movies.Poster || movies.Poster === "N/A" ? require('../../assets/movieIcon.png') : movies.Poster}
                                        alt={movies.Title}
                                        onClick= {() => this.props.watchedMovie(movies.imdbID)}
                                        />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="movie-details-container">
                        <div className="movie-title">
                            <div className="movie-data">{movies.Title}</div>
                        </div>
                        <div className="movie-released-date">
                            <div className="movie-data">{movies.Released}</div>
                        </div>
                        <div className="ratings-details">
                            <div className="rotten-details">
                                <div className="rotten-value">
                                    <div className="movie-data">{movies.Ratings[1] ? movies.Ratings[1].Value : "*"}</div>
                                </div>
                                <div className="rotten-source">
                                    <div className="movie-data">Rotten Tomatos</div>
                                </div>
                            </div>
                            <div className="imdb-details">
                                <div className="imdb-value">
                                    <div className="movie-data">{movies.imdbRating}</div>
                                </div>
                                <div className="imdb-source">
                                    <div className="movie-data">IMDB Rating</div>
                                </div>
                            </div>
                            <div className="watched">
                                <div className="movie-data">
                                    <button
                                        className={watchedStatus ? "watched-movie-status" : "default-watched-movie-status"}>Watched
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="movie-description-heading">
                            <div className="movie-data">Description</div>
                        </div>
                        <div className="movie-plot">
                            <div className="movie-data">{movies.Plot}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Movie