import React from 'react'
import './initialComponent.css'

const initialComponent = props => {
    return (
        <div className="text-container" style={{height:props.height,margin:props.margin}}>
            {props.error ? <p>{props.error}</p> : <p>{props.text}</p>}
        </div>
    )
}

export default initialComponent