import React from 'react';
import './timeline.css'
const timeline = props => {
    return (
        <div className="timeline-wrapper">
            <div className="current-item">
                <div className="image-container">
                    <div className="image">
                        <div className="image-shimmer">
                           <div className="animated-background"></div>
                        </div>
                    </div>
                </div>
                <div className="details-container">
                    <div className="movie-title">
                        <div className="animated-background"></div>
                    </div>
                    <div className="movie-released-date">
                        <div className="animated-background"></div>
                    </div>
                    <div className="ratings-details">
                        <div className="rotten-details">
                            <div className="rotten-value">
                                <div className="animated-background"></div>
                            </div>
                            <div className="rotten-source">
                                <div className="animated-background"></div>
                            </div>
                        </div>
                        <div className="imdb-details">
                            <div className="imdb-value">
                                <div className="animated-background"></div>
                            </div>
                            <div className="imdb-source">
                                <div className="animated-background"></div>
                            </div>
                        </div>
                        <div className="watched">
                            <div className="animated-background"></div>
                        </div>
                    </div>
                    <div className="movie-description-heading">
                        <div className="animated-background"></div>
                    </div>
                    <div className="movie-plot">
                        <div className="animated-background"></div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default timeline