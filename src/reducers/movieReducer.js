import * as actionTypes from '../actions/actionTypes'

export default (state={movies:{},watchedMovies:[], searchStringStatus:false, watchedMoviesId:[], errorMessage: null},action) => {
    switch (action.type) {
        case actionTypes.FETCH_MOVIES:
            return{
                ...state,
                movies: action.payload,
                errorMessage: action.error,
                searchStringStatus: false,
            }
        case actionTypes.WATCHED_MOVIE:
            return{
                ...state,
                watchedMovies: Array.from (new Set([...state.watchedMovies, action.payload])),
                watchedMoviesId: Array.from (new Set([...state.watchedMoviesId, action.payload.imdbID]))
            }
        case actionTypes.SEARCH_STRING_STATUS: 
            return{
                ...state,
                searchStringStatus: action.payload
            }
        case actionTypes.REMOVE_WATCHED_MOVIE: 
            return{
                ...state,
                watchedMovies: state.watchedMovies.filter(o => o.imdbID!== action.payload),
                watchedMoviesId: state.watchedMoviesId.filter(o => o!== action.payload)
            }
        default:
            return state
    }
}